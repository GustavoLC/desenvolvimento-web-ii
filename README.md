# Pokémon Gotta Catch'em All

# Como rodar o frontend:
- yarn
- yarn serve

# Como rodar o backend:
- dotnet run -> Para rodar a aplicação
            ou
- docker-compose up -d --build -> Para rodar o docker

# O que o programa faz?
- Teclas w, a, s & d movimentam o personagem.
- Pokémons irão aparecer em alguns momentos, tente capturá-los ao clicar no botão de capturar!
- Para ver os pokémons capturados acesse a Pokedex na parte superior.
    - Para acessar a Pokedex é necessário logar. O login padrão é: 'usuario' e 'senha'.
- Caso queira reiniciar o jogo e zerar os pokémons adquiridos basta clicar em Reiniciar Jogo!
- Agora, basta capturar todos os 151 Pokémons!

# Tecnologias:
- FrontEnd com VueJs, utilizando o Nuxt3.
- BackEnd com C#, DotNet 6.0
- Sistema de Containers com o Docker.
- Sistema de Login e JWT.

# API's
- https://app.swaggerhub.com/apis/GUSTAVOLC06_1/pokedex-api/1.0.0 - Pokedex API
- https://app.swaggerhub.com/apis/GUSTAVOLC06_1/capturar-pokemon_api/1.0.0 - CapturarPokemon API
- https://app.swaggerhub.com/apis/GUSTAVOLC06_1/move-player_api/1.0.0 - MovePlayer API
- https://app.swaggerhub.com/apis/GUSTAVOLC06_1/login-api/1.0.0 - Login API
- https://app.swaggerhub.com/apis/GUSTAVOLC06_1/token-validation_api/1.0.0 - Token Validation API
- https://app.swaggerhub.com/apis/GUSTAVOLC06_1/reset-game_api/1.0.0 - ResetGame API