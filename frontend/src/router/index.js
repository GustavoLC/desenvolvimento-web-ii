import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import axios from 'axios';

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/pokedex',
    name: 'pokedex',
    component: () => import('../views/PokedexView.vue'),
    meta: { requiresAuth: true },
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/LoginView.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    const token = localStorage.getItem('token');
    if (!token) {
      next('/login');
    } else {
      // Verifique o token no servidor antes de permitir o acesso à rota protegida
      axios.post('http://localhost:5023/api/auth/validatelogin', {
        token: token
      })
        .then(() => {
          next();
        })
        .catch(() => {
          next('/login');
        });
    }
  } else {
    next();
  }
});
export default router
