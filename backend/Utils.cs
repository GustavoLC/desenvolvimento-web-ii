using System.ComponentModel;
using System.Reflection;
using backend;
using MongoDB.Bson;
using MongoDB.Driver;

public static class Utils
{
    private static Random random = new Random();

    public static int SortearNumeroAleatorio(int numero)
    {
        return random.Next(1, numero + 1);
    }

    //Passa um numero inteiro (em %) para verificar se passou ou não na aleatoriedade
    public static bool Sortear(int numero)
    {
        return numero <= random.Next(1, 101);
    }

    public static Pokemon SortearPokemon(List<Pokemon> pPokemon)
    {
        var pokemonAtual = pPokemon[random.Next(0, pPokemon.Count-1)];

        //Caso seja lendário, sorteia novamente (lendário é raro)
        if ((new int[] { 144, 145, 146, 150, 151 }).Contains(pokemonAtual.NumeroPokedex))
            pokemonAtual = pPokemon[random.Next(0, pPokemon.Count-1)];
        
        return pokemonAtual;
    }

    public static List<Pokemon> ConstruirListaPokemons(List<BsonDocument> documentos)
    {
        List<Pokemon> pokemons = new List<Pokemon>();

        foreach (var document in documentos)
        {
            var pokemon = new Pokemon
            {
                Nome = document["nome"].AsString,
                NumeroPokedex = document["numero"].AsInt32,
                Descricao = document["descricao"].AsString,
                Tipo1 = (ETipoPokemon)document["tipo1"].AsInt32,
                Tipo2 = (ETipoPokemon)document["tipo2"].AsInt32,
                Capturado = document["capturado"].AsBoolean,
            };
            pokemon.ConstruirTipos();

            pokemons.Add(pokemon);
        }

        return pokemons;
    }

    public static Pokemon ConstruirPokemon(BsonDocument documento)
    {
        var pokemon = new Pokemon
        {
            Nome = documento["nome"].AsString,
            NumeroPokedex = documento["numero"].AsInt32,
            Descricao = documento["descricao"].AsString,
            Tipo1 = (ETipoPokemon)documento["tipo1"].AsInt32,
            Tipo2 = (ETipoPokemon)documento["tipo2"].AsInt32,
            Capturado = documento["capturado"].AsBoolean
        };
        pokemon.ConstruirTipos();

        return pokemon;
    }

    public static bool TentarCapturar(Pokemon pPokemon)
    {
        if (pPokemon.Capturado)
            return true;

        if (Sortear(40))
        {
            var mongoConnection = new MongoConnection();
            var _collection = mongoConnection.GetCollection<BsonDocument>("Pokemons");
            var cursor = _collection.AsQueryable();

            var filter = Builders<BsonDocument>.Filter.Eq("numero", pPokemon.NumeroPokedex);
            var update = Builders<BsonDocument>.Update
                .Set("capturado", true);

            _collection.UpdateOne(filter, update);

            return true;
        }

        return false;
    }

    public static string GetEnumDescription(Enum value)
    {
        FieldInfo fi = value.GetType().GetField(value.ToString());

        DescriptionAttribute[] attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

        if (attributes != null && attributes.Any())
        {
            return attributes.First().Description;
        }

        return value.ToString();
    }

    public static void ResetarCapturados()
    {
        var mongoConnection = new MongoConnection();
        var _collection = mongoConnection.GetCollection<BsonDocument>("Pokemons");
        var cursor = _collection.AsQueryable();

        var filter = Builders<BsonDocument>.Filter.Eq("capturado", true);
        var update = Builders<BsonDocument>.Update
            .Set("capturado", false);

        _collection.UpdateMany(filter, update);
    }
}