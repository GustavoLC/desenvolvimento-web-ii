
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace backend
{
    [ApiController]
    [Route("api/pokedex")]
    public class Pokedex
    {
        public List<Pokemon> _PokemonsCapturados { get; set; }
        public List<Pokemon> _PokemonsSelvagens { get; set; }

        public Pokedex()
        {

        }

        [HttpGet("get")]
        public string GetPokedex()
        {
            var mongoConnection = new MongoConnection();
            var collection = mongoConnection.GetCollection<BsonDocument>("Pokemons");
            var cursor = collection.AsQueryable();
            var listaPokemons = cursor.Where(d => d["capturado"] == true).ToList();
            return JsonConvert.SerializeObject(Utils.ConstruirListaPokemons(listaPokemons));
        }
    }
}
