using MongoDB.Driver;
using MongoDB.Bson;

namespace backend
{
    public class MongoConnection
    {
        private readonly IMongoDatabase _database;

        public MongoConnection()
        {
            var connectionString = "mongodb+srv://gustavolc:qyORQYv7MOmpKPZI@clusterteste.rohni7f.mongodb.net/?retryWrites=true&w=majority"; // Endereço do servidor MongoDB
            var databaseName = "PokemonGCA"; // Nome do banco de dados

            var client = new MongoClient(connectionString);
            _database = client.GetDatabase(databaseName);
        }

        public IMongoCollection<T> GetCollection<T>(string collectionName)
        {
            return _database.GetCollection<T>(collectionName);
        }
    }
}

// const string connectionUri = "mongodb+srv://gustavolc:<password>@clusterteste.rohni7f.mongodb.net/?retryWrites=true&w=majority";

// var settings = MongoClientSettings.FromConnectionString(connectionUri);

// // Set the ServerApi field of the settings object to Stable API version 1
// settings.ServerApi = new ServerApi(ServerApiVersion.V1);

// // Create a new client and connect to the server
// var client = new MongoClient(settings);

// // Send a ping to confirm a successful connection
// try {
//   var result = client.GetDatabase("admin").RunCommand<BsonDocument>(new BsonDocument("ping", 1));
//   Console.WriteLine("Pinged your deployment. You successfully connected to MongoDB!");
// } catch (Exception ex) {
//   Console.WriteLine(ex);
// }





