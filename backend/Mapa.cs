
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using MongoDB.Bson;
using MongoDB.Driver;

namespace backend
{
    public class Mapa
    {
        private IMongoCollection<BsonDocument> _collection;

        public Mapa() { }
        

        public Pokemon Mover(int pX, int pY)
        {
            if(Utils.Sortear(60))
                return null;

            //Retorna do banco
            var terrenoAtual = VerificarTerreno(pX, pY);

            return SortearPorTerreno(terrenoAtual);
        }

        private FilterDefinition<BsonDocument>? FiltrarPorTerreno(ETerrenos pTerreno)
        {
            List<ETipoPokemon> auxiliar = new List<ETipoPokemon>();

            //Excluo somente água
            if (pTerreno == ETerrenos.Grama)
            {
                auxiliar = new List<ETipoPokemon>() { ETipoPokemon.Agua };

                return Builders<BsonDocument>.Filter.And(
                    Builders<BsonDocument>.Filter.Nin("tipo1", auxiliar),
                    Builders<BsonDocument>.Filter.Nin("tipo2", auxiliar)
                );
            }
            else
            //Pego todos que tenha água no tipo1 ou tipo2
            if (pTerreno == ETerrenos.Agua)
            {
                return Builders<BsonDocument>.Filter.Or(
                    Builders<BsonDocument>.Filter.Eq("tipo1", ETipoPokemon.Agua),
                    Builders<BsonDocument>.Filter.Eq("tipo2", ETipoPokemon.Agua)
                );
            }

            return null;
        }

        private Pokemon SortearPorTerreno(ETerrenos pTerreno)
        {
            var mongoConnection = new MongoConnection();
            var filter = FiltrarPorTerreno(pTerreno);
            var collection = mongoConnection.GetCollection<BsonDocument>("Pokemons");

            var pokemons = collection.Find(filter).ToList();
            List<Pokemon> auxiliar = Utils.ConstruirListaPokemons(pokemons);

            return Utils.SortearPokemon(auxiliar);
        }

        private ETerrenos VerificarTerreno(int pX, int pY)
        {
            //Rect do rio principal
            if (pX >= 11 && pX <= 26 && 
                pY >= 9 && pY <= 14)
            {
                //Rect da grama dentro
                if (pX >= 15 && pX <= 22 &&
                    pY >= 11 && pY <= 12)
                {
                    return ETerrenos.Grama;
                }

                return ETerrenos.Agua;
            }

            return ETerrenos.Grama;
        }
    }
}
