public enum ETipoPokemon
{
    Nenhum = 0,
    Eletrico = 1,
    Agua = 2,
    Fogo = 3,
    Grama = 4,
    Gelo = 5,
    Lutador = 6,
    Venenoso = 7,
    Terra = 8,
    Voador = 9,
    Psiquico = 10,
    Inseto = 11,
    Pedra = 12,
    Fantasma = 13,
    Dragao = 14,
    Noturno = 15,
    Aco = 16,
    Fada = 17,
    Normal = 18
}