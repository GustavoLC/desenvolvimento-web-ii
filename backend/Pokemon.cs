
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace backend
{
    public class Pokemon
    {
        public int NumeroPokedex { get; set; }
        public string Descricao { get; set; }
        public string Nome { get; set; }
        public ETipoPokemon Tipo1 { get; set; }
        public ETipoPokemon Tipo2 { get; set; }
        public bool Capturado { get; set; }
        public string Tipo1_Str { get; set; }
        public string Tipo2_Str { get; set; }

        public Pokemon() { }

        public Pokemon(int pNumeroPokedex)
        {
            var mongoConnection = new MongoConnection();
            var _collection = mongoConnection.GetCollection<BsonDocument>("Pokemons");
            var cursor = _collection.AsQueryable();

            var documento = cursor.FirstOrDefault(d => d["numero"] == pNumeroPokedex);

            var pokemon = Utils.ConstruirPokemon(documento);

            //Atualizo as informações
            NumeroPokedex = pNumeroPokedex;
            Descricao = pokemon.Descricao;
            Nome = pokemon.Nome;
            Tipo1 = pokemon.Tipo1;
            Tipo2 = pokemon.Tipo2;
            Capturado = pokemon.Capturado;

            Tipo1_Str = Utils.GetEnumDescription(Tipo1);
            Tipo2_Str = Utils.GetEnumDescription(Tipo2);
        }

        public void ConstruirTipos()
        {
            Tipo1_Str = Utils.GetEnumDescription(Tipo1);
            Tipo2_Str = Utils.GetEnumDescription(Tipo2);
        }
    }
}
