using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace backend
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // Configurações e serviços necessários

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                {
                    builder.AllowAnyOrigin()
                           .AllowAnyMethod()
                           .AllowAnyHeader();
                });
            });

            services.AddControllers(); // Adiciona o serviço de controladores

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidIssuer = Configuration["Jwt:Issuer"],
                            ValidAudience = Configuration["Jwt:Audience"],
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("pNuvuPSZJoivvgib")),
                            RequireExpirationTime = true,
                            ClockSkew = TimeSpan.FromHours(2),
                        };
                    });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // for(int i = 1; i <= 5; i++)
            // {
            //     string caminho = @"/home/gustavo/Downloads/Pokedex/";
            //     var arquivo = File.ReadAllBytes(caminho + $"{i.ToString()}.png");
            //     Bitmap b = new Bitmap(img); 
            //     b = new Bitmap(b, new Size(50, 50));
            //     File.WriteAllBytes(caminho + $"{i.ToString()}teste.png", arquivo);
            // }

            var mapa = new Mapa();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors();

            // Outros middlewares e configurações de roteamento

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {

                });

                endpoints.MapGet("/mover/{xAtual}/{yAtual}", async context =>
                {
                    int x = Convert.ToInt32(context.Request.RouteValues["xAtual"]?.ToString());
                    int y = Convert.ToInt32(context.Request.RouteValues["yAtual"]?.ToString());

                    context.Response.WriteAsync(JsonConvert.SerializeObject(mapa.Mover(x, y)));
                });

                endpoints.MapGet("/capturar/{numero}", async context =>
                {
                    int numero = Convert.ToInt32(context.Request.RouteValues["numero"]?.ToString());

                    bool resultado = Utils.TentarCapturar(new Pokemon(numero));

                    context.Response.WriteAsJsonAsync(resultado);
                });
                
                endpoints.MapGet("/resetarJogo", async context =>
                {
                    Utils.ResetarCapturados();
                });

                endpoints.MapControllers();
            });
        }
    }
}