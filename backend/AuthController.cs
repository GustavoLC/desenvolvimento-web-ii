using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Text.Json;
using System.Text.Json.Serialization;

[ApiController]
[Route("api/auth")]
public class AuthController : ControllerBase
{
    private const string SecretKey = "pNuvuPSZJoivvgib";

    [HttpPost("login")]
    public IActionResult Login([FromBody] LoginRequest request)
    {
        // Verifique as credenciais do usuário no banco de dados ou em outro local seguro
        if (request.Username == "usuario" && request.Password == "senha")
        {
            var token = GenerateToken(request.Username);
            return Ok(new { token });
        }

        return Unauthorized();
    }

    private string GenerateToken(string username)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(SecretKey);

        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, username)
            }),
            Expires = DateTime.UtcNow.AddHours(2),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        };

        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }

    [HttpPost("validatelogin")]
    public ClaimsPrincipal ValidateToken([FromBody] TokenGCA tokenRequest)
    {
        var token = tokenRequest.Token;
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(SecretKey);
        var validationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(key),
            ValidateIssuer = false,
            ValidateAudience = false
        };

        try
        {
            // Configurar JsonSerializerOptions com ReferenceHandler.Preserve
            var jsonSerializerOptions = new JsonSerializerOptions
            {
                ReferenceHandler = ReferenceHandler.Preserve
            };

            // Serializar o objeto para JSON com as opções configuradas
            var tokenRequestJson = JsonSerializer.Serialize(tokenRequest, jsonSerializerOptions);

            // Validar o token
            return tokenHandler.ValidateToken(tokenRequestJson, validationParameters, out _);
        }
        catch
        {
            return null;
        }
    }
}